const csvFilePath = "goodFood.csv";
const csv = require("csvtojson");
const axios = require("axios");

const importCsv = async () => {
  const jsonArray = await csv().fromFile(csvFilePath);
  console.log(jsonArray);
};

importCsv();
